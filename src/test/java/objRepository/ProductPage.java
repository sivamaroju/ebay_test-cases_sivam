package objRepository;

import org.openqa.selenium.By;

public class ProductPage {

	public static By btnAddToCart = new By.ByXPath("//*[@id='isCartBtn_btn']");
	public static By btnProtecPlan = new By.ByXPath("//*[@id='ADDON_0']/div/div[2]/div/div[4]/div/button[1]");
	public static By lstOption = new By.ByXPath("//*[@id='msku-sel-1']");
	public static By btnCheckOut = new By.ByXPath("//*[@id='mainContent']/div/div[3]/div/div/button");
	
}
