package objRepository;

import org.openqa.selenium.By;

public class RegisterPage {

	public static By txtFName = new By.ByXPath("//*[@id='firstname']");
	public static By txtLName = new By.ByXPath("//*[@id='lastname']");
	public static By txtEmail = new By.ByXPath("//*[@id='email']");
	public static By txtPwd = new By.ByXPath("//*[@id='PASSWORD']");
	public static By btnRegister = new By.ByXPath("//*[@id='ppaFormSbtBtn']");
	
}
