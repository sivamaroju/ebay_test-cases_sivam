/* This test case is to verify customer, is able to register with valid details */
package testScripts;

import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import configurations.StartBrowser;
import driverCommands.DriverActions;
import objRepository.HomePage;
import objRepository.RegisterPage;

public class TC1_RegisterUser_Verification extends StartBrowser{
  @Test
  
  
  public void testRegisterFunctionality() {
	  DriverActions adriver = new DriverActions();  
	  adriver.launchApplication("https://www.ebay.com.au/");
	  adriver.click(HomePage.lnkRegister);
	  adriver.enterData(RegisterPage.txtFName, "Siva");
	  adriver.enterData(RegisterPage.txtLName, "M");
	  adriver.enterData(RegisterPage.txtEmail, "Cust2088@hotmail.com");
	  adriver.enterData(RegisterPage.txtPwd, "Cust2019");
	  WebDriverWait w = new WebDriverWait(driver, 5);
	  w.until(ExpectedConditions.elementToBeClickable(RegisterPage.btnRegister));
	  adriver.click(RegisterPage.btnRegister);
  }
}
