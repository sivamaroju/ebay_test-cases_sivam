/* This Test Case is to verify a customer with valid credentials , is able to login and search a product and add to cart
 * PLEASE NOTE THAT I HAVE ADDED LOGIN FUNCTIONALITY HERE EVEN THOUGH THERE'S TC2 EXISTS, REASON BEING TC2 VERIFIES BOTH LOGIN AND LOGOUT*/

package testScripts;

import org.testng.annotations.Test;
import org.testng.Assert;
import configurations.StartBrowser;
import driverCommands.DriverActions;
import objRepository.HomePage;
import objRepository.LoginPage;
import objRepository.ProductPage;
import objRepository.SearchPage;

public class TC4_SearchAndAdd_Verification extends StartBrowser{
	
  @Test
  public void verifySearchAndCart() {
	  DriverActions adriver = new DriverActions(); 
		adriver.launchApplication("https://www.ebay.com.au/");
		adriver.click(HomePage.lnkSignIn);
		adriver.enterData(LoginPage.txtEmail, "Cust2088@hotmail.com");
		adriver.enterData(LoginPage.txtPwd, "Cust2019");
		adriver.click(LoginPage.btnLogin);
		adriver.enterData(SearchPage.txtSearch, "bose headphones");
		adriver.click(SearchPage.btnSearch);
		adriver.click(SearchPage.lnkFirstResult);
		adriver.selectDropDwn(ProductPage.lstOption, 1);
		adriver.click(ProductPage.btnAddToCart);
		adriver.click(ProductPage.btnProtecPlan);
		String msg=adriver.getText(ProductPage.btnCheckOut);
		Assert.assertEquals(msg, "Go to checkout");
		
  }
}
