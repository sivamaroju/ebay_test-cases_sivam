/* This Test case is to verify , when a customer enters invalid credentials , the login fails */
package testScripts;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


import configurations.StartBrowser;
import driverCommands.DriverActions;
import junit.framework.Assert;
import objRepository.HomePage;
import objRepository.LoginPage;

public class TC3_InvalidLogin_Verification extends StartBrowser{
	@Test
	public void loginUser()
	{
		String expError ="Oops, that's not a match.";
		DriverActions adriver = new DriverActions();  
		adriver.launchApplication("https://www.ebay.com.au/");
		adriver.click(HomePage.lnkSignIn);
		adriver.enterData(LoginPage.txtEmail, "Cust2088@hotmail.com");
		adriver.enterData(LoginPage.txtPwd, "Cust2009");
		adriver.click(LoginPage.btnLogin);
		 WebDriverWait w = new WebDriverWait(driver, 5);
		w.until(ExpectedConditions.elementToBeClickable(LoginPage.btnLogin));
		String eMsg=adriver.getText(LoginPage.txtEmsg);
		Assert.assertEquals(expError, eMsg);
	}

}
