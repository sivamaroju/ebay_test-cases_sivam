/* This test case is to verify, a customer is able to login the application with valid credentials */
package testScripts;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

import configurations.StartBrowser;
import driverCommands.DriverActions;
import objRepository.HomePage;
import objRepository.LoginPage;

public class TC2_LoginUser_Verification extends StartBrowser{
	
	@Test
	public void loginUser()
	{	DriverActions adriver = new DriverActions();  
		adriver.launchApplication("https://www.ebay.com.au/");
		adriver.click(HomePage.lnkSignIn);
		adriver.enterData(LoginPage.txtEmail, "Cust2088@hotmail.com");
		adriver.enterData(LoginPage.txtPwd, "Cust2019");
		adriver.click(LoginPage.btnLogin);
		adriver.mouseHover(HomePage.lnkMyAccount);
		adriver.click(HomePage.btnSignOut);
	}
	

}
