package driverCommands;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import configurations.StartBrowser;

public class DriverActions {
	
	WebDriver driver;
	
	public DriverActions()
	{
		driver = StartBrowser.driver;
		
	}
	
	/**
	 * This method is to launch an application
	 * parameter: url
	 * Example - https://www.ebay.com.au/
	 */
	public void launchApplication(String url)
	{
		driver.get(url);
	}
	
	/**
	 * This method is to click a button
	 * @param locator
	 */
public void click(By locator)
{
	driver.findElement(locator).click();
	
}
/**
 * this method is to enter data in a text box
 * @param locator
 * @param testData
 */

public void enterData(By locator, String testData)
{
	driver.findElement(locator).sendKeys(testData);
}
	
public void mouseHover(By locator)
{
	Actions a = new Actions(driver);
	WebElement mo = driver.findElement(locator);
	a.moveToElement(mo).build().perform();
	
}

public String getText(By locator)
{
	String eMsg = driver.findElement(locator).getText();
	return eMsg;
}

public void selectDropDwn (By locator,int itemNo)
{
	Select options = new Select(driver.findElement(locator));
	options.selectByIndex(itemNo);
}
}
